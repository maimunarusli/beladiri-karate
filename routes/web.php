<?php

use App\Http\Controllers\SabukhitamController;
use App\Models\sabukhitam;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');

});
// Route::get('/pendaftaran', function () {
//     return view('pendaftaran');
    
// });
Route::get('/profile', function () {
    return view('profile');
    
});
Route::get('/tampilan', function () {
    return view('tampilan');
    
});
Route::get('/tentang', function () {
    return view('tentang');
    
});


route::get('list' , [SabukhitamController::class , 'index'])->name('list');
route::get('pendaftaran' , [SabukhitamController::class , 'create'])->name('daftar');
route::post('pendaftaran' , [SabukhitamController::class , 'store'])->name('daftar');
route::get('update/{id}' , [SabukhitamController::class , 'edit'])->name('update');
route::put('update/{id}' , [SabukhitamController::class , 'update'])->name('update');
route::delete('delete/{id}' , [SabukhitamController::class , 'destroy'])->name('delete');
// route::post('simpan_anggota', [SabukhitamController::class , 'store'])->name('store');
// route::get('tampil/{id}' , [SabukhitamController::class , 'show'])->name('tampil');




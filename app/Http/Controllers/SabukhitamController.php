<?php

namespace App\Http\Controllers;

use App\Models\sabukhitam;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SabukhitamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = sabukhitam::all();
        return view('list' , compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pendaftaran');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new sabukhitam;
        $save = $data->create([
                'nama' => $request->nama,
                'tetala' => Carbon::parse($request->tetala),
                'alamat' => $request->alamat,
                'no_hp'=> $request->no_hp,
                'jurusan'=>$request->jurusan,
                'agama'=>$request->agama,
                'tujuan'=>$request->tujuan,
        ]);
        if ($save){
            $data = sabukhitam::all();
            return view('list' , compact('data'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\sabukhitam  $sabukhitam
     * @return \Illuminate\Http\Response
     */
    public function show(sabukhitam $sabukhitam)
    {
        // $sabuk = sabukhitam::all();

        // return view('pendaftaran',compact('sabuk'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\sabukhitam  $sabukhitam
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = sabukhitam::find($id);
        return view('edit' , compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\sabukhitam  $sabukhitam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = sabukhitam::find($id);
        
        $save = $data->update([
            'nama' => $request->nama,
                'tetala' => Carbon::parse($request->tetala),
                'alamat' => $request->alamat,
                'no_hp'=> $request->no_hp,
                'jurusan'=>$request->jurusan,
                'agama'=>$request->agama,
                'tujuan'=>$request->tujuan,
        ]);
        if($save){
            $data = sabukhitam::all();
            return view('list' , compact('data'));
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\sabukhitam  $sabukhitam
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = sabukhitam::find($id);
        $data->delete();
        if ($data){
            $data = sabukhitam::all();
            return view('list' , compact('data'));
        }
    }
}

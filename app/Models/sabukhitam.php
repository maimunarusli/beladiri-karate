<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class sabukhitam extends Model
{
    use HasFactory;
    protected $fillable =([
        'nama',
        'tetala',
        'alamat',
        'no_hp',
        'jurusan',
        'agama',
        'tujuan',
    ]);
}

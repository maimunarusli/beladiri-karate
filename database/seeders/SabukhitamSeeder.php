<?php

namespace Database\Seeders;

use App\Models\sabukhitam;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SabukhitamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new sabukhitam();
        $data ->create([
            'nama' => 'MaimunaRusli',
            'no_hp' => '082132654467',
            'alamat' => 'batioh banyuates',
            'jurusan' => 'tekhnik',
            'agama' => 'Islam',
            'tujuan' => 'sebagai pertahanan diri sendiri',
            'tetala' => Carbon::parse('12-11-2000'),
        ]);
    }
}

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>Karate Universitas Islam Madura</title>

   

    

    <!-- Bootstrap core CSS -->
<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="{{asset('product.css')}}" rel="stylesheet">
  </head>
  <body>
    
<header class="site-header sticky-top py-1">
  <nav class="container d-flex flex-column flex-md-row justify-content-between">
    <a class="py-2" href="/" aria-label="Product">
      <img src="{{asset('icon.png')}}" alt="icon" style="width: 25px; height: 25px; border-radius: 50px">
    </a>
    <a class="py-2 d-none d-md-inline-block" href="/profile">profile</a>
    <a class="py-2 d-none d-md-inline-block" href="/tampilan">Inkai</a>
    <a class="py-2 d-none d-md-inline-block" href="/pendaftaran">pendaftaran</a>
    <a class="py-2 d-none d-md-inline-block" href="/tentang">Tehnikdasar</a>
   
  </nav>
</header>

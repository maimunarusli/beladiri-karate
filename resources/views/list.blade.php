@extends('content')

@section('container')
    <div class="container-fluid">
        <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Tetala</th>
            <th scope="col">Alamat</th>
            <th scope="col">No.Hp</th>
            <th scope="col">Jurusan</th>
            <th scope="col">Agama</th>
            <th scope="col">Tujuan</th>
            <th scope="col">Aksi</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $item) 
        <tr>
            <th scope="row">{{$loop->iteration}}</th>
            <td>{{$item->nama}}</td>
            <td>{{$item->tetala}}</td>
            <td>{{$item->alamat}}</td>
            <td>{{$item->no_hp}}</td>
            <td>{{$item->jurusan}}</td>
            <td>{{$item->agama}}</td>
            <td>{{$item->tujuan}}</td>
            <td>
                <a href="{{route('update' , $item->id)}}">
                    <button type="button" class="btn btn-success btn-sm">Edit</button>
                </a>
                <form method="POST" action="{{ route('delete', $item->id) }}" id="hapus">
                    @csrf
                    @method('DELETE')
                    <div class="d-grid gap-2">
                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('yakin?')">Hapus</button>
                    </div>
                </form>            
            </td>
            
        </tr>
        @endforeach
        </tbody>
        </table>
    </div>
@endsection


@extends('content')
@section('container')
    

<body >
    <h1 style="text-align: center;padding-top:30px;">INSTITUT KARATE-DO INDONESIA</h1>
    <h1 align ="center">Ranting Universitas Islam Madura Pamekasan</h1>

    <img src="karate.png" alt="karate" width="1450", height="300">
    <p style="text-indent: 100px; padding-top:20px;">
        INKAI Ranting Universitas Islam Madura didirikan berdasarkan SK Pengurus Cabang INKAI Pamekasan nomor 02/INKAI-PMK/SK-RAN/I/2021 tentang pengesahan pengurus ranting INKAI Universitas Islam Madura periode 2021 – 2023. 
         Susunan Pengurus INKAI Ranting Universitas Islam Madura Periode 2021 – 2023
         Sejarah berdirinya Institut Karate-Do Indonesia (INKAI) tidak terlepas dari sejarah perkembangan seni bela diri karate di Indonesia. Setelah PORKI terpecah pada tahun 1970, maka berdirilah beberapa perguruan shotokan yang didirikan oleh alumni JKA (Japan Karate Associations) 
         seperti :
            LEMKARI yang didirikan oleh Anton Lesiangi
            INKADO yang didirikan oleh Alm. Baud Adikusumo
            INKAI yang didirikan oleh Sabeth Muchsin
            Berdirinya INKAI berawal dari rapat yang dilaksanakan di jalan Matraman Dalam Jakarta Pusat, pada tanggal 15 April 1971 (yang akhirnya disepakati sebagai hari lahirnya INKAI). Dalam rapat yang berlangsung dari mulai pukul 09.00 hingga 18.00 WIB tersebut dihadiri oleh beberapa karateka eks PORKI seperti, Sabeth Muchsin, Nico A. Lumenta ( Tuan Rumah ), Abdul Latief, Sori Tua Hutagalung (alm.), Albert L. Tobing, Wono Sarono, A.S Siregar ( alm.) dan salah satu karateka INKAI yang belakangan diketahui sebagai pembuat dan menggambar lambang INKAI bernama Harsono Rubio (alm.)

            bela-diri-inkai
            faradillasn.blogspot.com
            Setelah selesai rapat maka hasilnya adalah ketua umum INKAI pertama yaitu Letjend G.H. Mantik dan sebagai ketua Dewan Guru INKAI pertama adalah Sabeth Muchsin. Dalam sejarahnya INKAI telah memalui banyak rintangan, namun itu semua berhasil dilewati dan telah berhasi meraih berbagai prestasi yang mengharumkan nama bangsa.

            Tanggal 25 Mei 1971, INKAI resmi berdiri sebagai perguruan anggota FORKI dan oleh PB FORKI, INKAI ditunjuk mewakili Indonesia mengikuti kejuaraan karate WUKO 1 di Jepang. Dan Inkai juga merupakan anggota resmi  afiliasi JKA yang bekedudukan di Jepang.
    </p> 
    <h1 style="text-align: center";>Visi </h1>
    <p style="text-indent:100px;">Mewujudkan UKM INKAI UM sebagai wadah pembentukan mahasiswa berkarakter dan berjiwa bushido (kesatria) dalam rangka mewujudkan insan yang berkepribadian luhur, berbudi pekerti dan jujur, memiliki daya juang dan keahlian yang tinggi, menjunjung tinggi nilai-nilai etika.</p>
    <h1 style="text-align: center";>Misi</h1>
    
    <p style="text-indent:100px;">

             Mengusahakan serta memaksimalkan sarana dan prasarana.
            Membina dan mendidik semua anggota UKM INKAI UM sesuai dengan prinsip Karate-Do dan Sumpah Karate.
            Mengembangkan Karate-Do secara ilmiah.
            Memupuk semangat persatuan dan kesatuan serta mengadakan kerjasama dengan seluruh organisasi beladiri yang ada di seluruh Indonesia pada umumnya, dan organisasi beladiri yang ada di Universitas Negeri Malang pada khususnya.
            Mengadakan hubungan kerjasama dengan badan-badan pemerintah swasta, perguruan / aliran karate di seluruh Indonesia yang berkaitan dengan Karate-Do.
    </p>

</body>

    

@endsection
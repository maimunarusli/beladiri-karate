@extends('content')
@section('container')

<main>
  <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
    <div class="col-md-5 p-lg-5 mx-auto my-5">
      <h1 class="display-4 fw-normal">KARATE-DO</h1>
      <p class="lead fw-normal">kARATE INKAI&FORKI Universitas Islam Universitas Pamekasan</p>
      <a class="btn btn-outline-secondary" href="/pendaftaran">Coming soon</a>
    </div>
    <div class="product-device shadow-sm d-none d-md-block"></div>
    <div class="product-device product-device-2 shadow-sm d-none d-md-block"></div>
  </div>

  <main>
  
    <div class="d-md-flex flex-md-equal w-100 my-md-3 ps-md-3">
      <div class="bg-dark me-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center text-white overflow-hidden">
        <div class="my-3 py-3">
          <h2 class="display-5"></h2>
          <p class="lead">Karate adalah seni beladiri yang berasal dari daratan cina yang akhirnya berkembang dan populer di Jepang. Rata rata gerakan di Karate mengandalkan kekuatan tangan. Teknik Karate terbagi menjadi tiga bagian utama : Kihon (teknik dasar), Kata(jurus) dan Kumite (pertarungan).
            INKAI Ranting Universitas Islam Madura didirikan berdasarkan SK Pengurus Cabang INKAI Pamekasan nomor 02/INKAI-PMK/SK-RAN/I/2021 tentang pengesahan pengurus ranting INKAI Universitas Islam Madura periode 2021 – 2023. 
          </p>
        </div>
        
  
</main>

@endsection

